FROM openanolis/anolisos:8.6

LABEL maintainer="OpenAnolis Cloud Native SIG"

RUN yum -y update \
	&& yum -y install redis wget \
	&& yum clean all

ARG TARGETARCH

ENV GOSU_VERSION 1.14

RUN set -eux; \
        wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$TARGETARCH"; \
        wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$TARGETARCH.asc"; \
        export GNUPGHOME="$(mktemp -d)"; \
        gpg --batch --keyserver pgp.mit.edu --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
        gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
        gpgconf --kill all; \
        rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
        chmod +x /usr/local/bin/gosu; \
        gosu --version; \
        gosu nobody true

RUN set -eux; \
	sed -e "/^protected-mode/s/yes/no/" \
	    -e "s/^logfile/#logfile/" \
	    -e "s/^bind/#bind/" \
	    -i /etc/redis.conf; \
	redis-cli --version; \
	redis-server --version

RUN mkdir /data && chown redis:redis /data

ENV REDIS_VERSION 5.0.3

VOLUME /data

WORKDIR /data

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 6379

CMD ["redis-server", "/etc/redis.conf"]
