# OpenAnolis Container Images

## Introduction
This repository contains Dockerfiles for OpenAnolis official container images, including Anolis OS base images, and application images.

## Anolis OS Base Images
Official Anolis OS base image published by OpenAnolis community.

### Supported tags and respective `Dockerfile` links
- [`8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/anolisos/8.6/Dockerfile)

### Supported architectures
- x86\_64, aarch64

## OpenAnolis Application Images
Popular application images published by OpenAnolis community.

### Application Images Rules

#### Dockerfile Rules
- Path: `[Application Name]/[Application Version]/[Base Image Version]/Dockerfile`

#### Readme Rules
- Path: `[Application Name]/Readme.md`

#### Supported tags Rules
- Release version: `[Application Version]-[Base Image Version]`

## Available Container Images Repository
### Docker Hub
- Website: https://hub.docker.com/r/openanolis/
- Download: `docker pull openanolis/[Image Name]:[Image Tag]`

### Aliyun ACR
- Download: `docker pull cloud-native-sig-registry.cn-hangzhou.cr.aliyuncs.com/openanolis/[Image Name]:[Image Tag]`
