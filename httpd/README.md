# Httpd

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`2.4.37-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/httpd/2.4.37/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/httpd:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage
```shell
docker run -dit --name my-apache-app -p 8080:80 -v "$PWD":/var/www/html/ openanolis/httpd
```

# Httpd
The Apache HTTP Server, colloquially called Apache, is a Web server application notable for playing a key role in the initial growth of the World Wide Web. Originally based on the NCSA HTTPd server, development of Apache began in early 1995 after work on the NCSA code stalled. Apache quickly overtook NCSA HTTPd as the dominant HTTP server, and has remained the most popular HTTP server in use since April 1996.
