# Nginx

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`1.14.1-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/nginx/1.14.1/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/nginx:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a nginx instance
```shell
docker run --name some-nginx -d -p 8080:80 openanolis/nginx
```

# Nginx
Nginx (pronounced "engine-x") is an open source reverse proxy server for HTTP, HTTPS, SMTP, POP3, and IMAP protocols, as well as a load balancer, HTTP cache, and a web server (origin server).
